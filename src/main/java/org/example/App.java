package org.example;

import jdbc.entities.*;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //base
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("pu-petstore");
        EntityManager em = entityManagerFactory.createEntityManager();
        System.out.println(em.toString());

        EntityTransaction et = em.getTransaction();
        et.begin();
        PetStore store = new PetStore();

        //création adresse
        Adresse adresse = new Adresse();
        adresse.setCity("Rennes");
        adresse.setNumber("14");
        adresse.setPetStore(store);
        adresse.setStreet("rue des Lilas");
        adresse.setZipCode("derftghj");
        store.setAdresse(adresse);

        store.setManagerName("Anderson");

        //création produit
        Set<Product> productSet = new HashSet<Product>();
        Set<PetStore> stores = new HashSet<PetStore>();
        stores.add(store);

        //produit 1
        Product produit1 = new Product();
        produit1.setCode("20");
        produit1.setLabel("truc muche");
        produit1.setPrice(80);
        produit1.setType(ProdType.ACCESSORY);
        produit1.setPetStores(stores);
        productSet.add(produit1);

        //produit 2
        Product produit2 = new Product();
        produit2.setCode("60");
        produit2.setLabel("bidule muche");
        produit2.setPrice(8);
        produit2.setType(ProdType.CLENING);
        produit2.setPetStores(stores);
        productSet.add(produit2);

        //produit 3
        Product produit3 = new Product();
        produit3.setCode("30");
        produit3.setLabel("bouffe");
        produit3.setPrice(15);
        produit3.setType(ProdType.FOOD);
        produit3.setPetStores(stores);
        productSet.add(produit3);


        store.setProducts(productSet);

        //création animal
        Set<Animal> animals = new HashSet<Animal>();
        Fish fish1 = new Fish();
        fish1.setLivingEnv(FishLivEnv.FRESH_WATER);
        fish1.setBirth(new Date(System.currentTimeMillis()));
        fish1.setPetStore(store);
        fish1.setCouleur("red");
        animals.add(fish1);

        Cat cat1 = new Cat();
        cat1.setChipId("fghjkl");
        cat1.setBirth(new Date(System.currentTimeMillis()));
        cat1.setPetStore(store);
        cat1.setCouleur("blue");
        animals.add(cat1);

        Animal animal1 = new Animal();
        animal1.setBirth(new Date(System.currentTimeMillis()));
        animal1.setPetStore(store);
        animal1.setCouleur("black");
        animals.add(animal1);


        store.setAnimals(animals);
        store.setNom("pet's haven");
        em.persist(store);

        //store 2
        PetStore store2 = new PetStore();

        //création adresse
        Adresse adresse2 = new Adresse();
        adresse2.setCity("Nantes");
        adresse2.setNumber("28");
        adresse2.setPetStore(store2);
        adresse2.setStreet("rue des truc");
        adresse2.setZipCode("bruh");
        store2.setAdresse(adresse2);

        //création produit
        Set<Product> productSet2 = new HashSet<Product>();
        stores.add(store2);

        //produit 4
        Product produit4 = new Product();
        produit4.setCode("280");
        produit4.setLabel("truc muche bidule");
        produit4.setPrice(75);
        produit4.setType(ProdType.ACCESSORY);
        produit4.setPetStores(stores);
        productSet2.add(produit4);

        //produit 5
        Product produit5 = new Product();
        produit5.setCode("55");
        produit5.setLabel("bidule machin muche");
        produit5.setPrice(88);
        produit5.setType(ProdType.CLENING);
        produit5.setPetStores(stores);
        productSet2.add(produit5);

        //produit 6
        Product produit6 = new Product();
        produit6.setCode("3963");
        produit6.setLabel("bouffe machin");
        produit6.setPrice(155);
        produit6.setType(ProdType.FOOD);
        produit6.setPetStores(stores);
        productSet2.add(produit6);


        store2.setProducts(productSet2);

        //création animal
        Set<Animal> animals2 = new HashSet<Animal>();
        Fish fish2 = new Fish();
        fish2.setLivingEnv(FishLivEnv.SEA_WATER);
        fish2.setBirth(new Date(System.currentTimeMillis()));
        fish2.setPetStore(store2);
        fish2.setCouleur("violet");
        animals2.add(fish2);

        Cat cat2 = new Cat();
        cat2.setChipId("pljiuo");
        cat2.setBirth(new Date(System.currentTimeMillis()));
        cat2.setPetStore(store2);
        cat2.setCouleur("yellow");
        animals2.add(cat2);

        Animal animal2 = new Animal();
        animal2.setBirth(new Date(System.currentTimeMillis()));
        animal2.setPetStore(store2);
        animal2.setCouleur("grey");
        animals2.add(animal2);


        store2.setAnimals(animals2);
        store2.setNom("pet's hell");
        store2.setManagerName("Cendre");
        em.persist(store2);

        PetStore store3 = new PetStore();

        //création adresse
        Adresse adresse3 = new Adresse();
        adresse3.setCity("Nantes");
        adresse3.setNumber("28");
        adresse3.setPetStore(store2);
        adresse3.setStreet("rue des truc");
        adresse3.setZipCode("bruh");
        store3.setAdresse(adresse2);

        store3.setManagerName("Alastor");

        //création produit
        Set<Product> productSet3 = new HashSet<Product>();
        stores.add(store3);

        //produit 7
        Product produit7 = new Product();
        produit7.setCode("2806");
        produit7.setLabel("plus d'idée");
        produit7.setPrice(785);
        produit7.setType(ProdType.ACCESSORY);
        produit7.setPetStores(stores);
        productSet3.add(produit7);

        //produit 8
        Product produit8 = new Product();
        produit8.setCode("555");
        produit8.setLabel("toujours pas d'idée");
        produit8.setPrice(858);
        produit8.setType(ProdType.CLENING);
        produit8.setPetStores(stores);
        productSet3.add(produit8);

        //produit 9
        Product produit9 = new Product();
        produit9.setCode("35963");
        produit9.setLabel("non j'ai pas d'idée !");
        produit9.setPrice(1555);
        produit9.setType(ProdType.FOOD);
        produit9.setPetStores(stores);
        productSet3.add(produit9);


        store3.setProducts(productSet3);

        //création animal
        Set<Animal> animals3 = new HashSet<Animal>();
        Fish fish3 = new Fish();
        fish3.setLivingEnv(FishLivEnv.FRESH_WATER);
        fish3.setBirth(new Date(System.currentTimeMillis()));
        fish3.setPetStore(store3);
        fish3.setCouleur("blanc");
        animals3.add(fish3);

        Cat cat3 = new Cat();
        cat3.setChipId("Ph'nglui mglw'nqfh Cthulhu R'lyeh wgah'nagl fhtagn");
        cat3.setBirth(new Date(System.currentTimeMillis()));
        cat3.setPetStore(store3);
        cat3.setCouleur("vert");
        animals3.add(cat3);

        Animal animal3 = new Animal();
        animal3.setBirth(new Date(System.currentTimeMillis()));
        animal3.setPetStore(store3);
        animal3.setCouleur("rose");
        animals3.add(animal3);


        store3.setAnimals(animals3);
        store3.setNom("Hazbin Hotel");
        em.persist(store3);
        et.commit();

        PetStore petStore = em.find(PetStore.class, 3l);
        Set<Animal> donne = petStore.getAnimals();
        System.out.println(donne.toString());
        entityManagerFactory.close();
        em.close();
    }
}
