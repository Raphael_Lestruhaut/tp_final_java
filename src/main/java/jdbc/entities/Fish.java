package jdbc.entities;

import javax.persistence.*;

@Entity
public class Fish extends Animal
{
    @Column(name = "LIVINGENV")
    @Enumerated(EnumType.ORDINAL)
    private FishLivEnv livingEnv;

    public Fish()
    {

    }

    public FishLivEnv getLivingEnv()
    {
        return livingEnv;
    }

    public void setLivingEnv(FishLivEnv livingEnv)
    {
        this.livingEnv = livingEnv;
    }

    @Override
    public String toString()
    {
        return "Fish{" +
                "livingEnv=" + livingEnv +
                '}';
    }
}
