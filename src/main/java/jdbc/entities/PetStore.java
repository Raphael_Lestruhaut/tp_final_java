package jdbc.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "pet_store")
public class PetStore
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NOM")
    private String nom;
    @Column(name = "MANAGERNAME")
    private String managerName;
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "store_product",
            joinColumns = @JoinColumn(name = "ID_STORE", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ID_PRODUCT", referencedColumnName = "ID")
    )
    private Set<Product> products;
    @OneToMany(mappedBy = "petStore",cascade = CascadeType.PERSIST)
    private Set<Animal> animals;
    @OneToOne(mappedBy = "petStore",cascade = CascadeType.PERSIST)
    private Adresse adresse;

    public PetStore()
    {

    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public String getManagerName()
    {
        return managerName;
    }

    public void setManagerName(String managerName)
    {
        this.managerName = managerName;
    }

    public Set<Product> getProducts()
    {
        return products;
    }

    public void setProducts(Set<Product> products)
    {
        this.products = products;
    }

    public Set<Animal> getAnimals()
    {
        return animals;
    }

    public void setAnimals(Set<Animal> animals)
    {
        this.animals = animals;
    }

    public Adresse getAdresse()
    {
        return adresse;
    }

    public void setAdresse(Adresse adresse)
    {
        this.adresse = adresse;
    }

    @Override
    public String toString()
    {
        return "PetStore{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", managerName='" + managerName + '\'' +
                '}';
    }
}
